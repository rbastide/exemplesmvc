<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Formmulaire de saisie</title>
	</head>
	<body>
		<h1>Formulaire BeanParam</h1>
		<%-- L'action par défaut est de revenir au contôleur --%>
		<form method="POST"> 
			Votre nom :   <input name="nom" value="${beanParam.nom}">
			<ul> <%-- On montre les erreurs de saisie éventuelles --%>
			<c:forEach var="error" items="${validationErrors.getErrors('nom')}">
				<li><span style="color: red;">${error.message}</span></li>
			</c:forEach>
			</ul>

			Votre email : <input name="email"  value="${beanParam.email}">
			<ul> <%-- On montre les erreurs de saisie éventuelles --%>
			<c:forEach var="error" items="${validationErrors.getErrors('email')}">
				<li><span style="color: red;">${error.message}</span></li>
			</c:forEach>
			</ul>		
			Votre date de naissance : <input name="naissance" type="date"  value="${beanParam.naissance.htmlValue}">
			<ul> <%-- On montre les erreurs de saisie éventuelles --%>
			<c:forEach var="error" items="${validationErrors.getErrors('naissance')}">
				<li><span style="color: red;">${error.message}</span></li>
			</c:forEach>
			</ul>

			Votre salaire : <input name="salaire" type="number" value="${beanParam.salaire}">
			<ul> <%-- On montre les erreurs de saisie éventuelles --%>
			<c:forEach var="error" items="${validationErrors.getErrors('salaire')}">
				<li><span style="color: red;">${error.message}</span></li>
			</c:forEach>
			</ul>		
			<input type="submit">
		</form>
		<a href="${pageContext.request.contextPath}">Retour au menu</a>	
	</body>
</html>
