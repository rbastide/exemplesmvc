<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Formmulaire de saisie</title>
    </head>
    <body>
        <h1>Bonjour</h1>
	<%-- L'action par défaut est de revenir au contôleur --%>
	<form method="POST"> 
		Vous êtes :
		<select name="sexe">
			<option value="F">Une femme</option>
			<option value="M">Un  homme</option>			
		</select>
		<input type="submit">
	</form>
	<a href="${pageContext.request.contextPath}">Retour au menu</a>	
    </body>
</html>
