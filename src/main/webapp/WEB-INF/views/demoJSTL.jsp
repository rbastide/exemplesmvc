<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Tags JSTL</title>
	</head>
	<body>
		<h1>Bonjour
			<c:choose>
				<c:when test="${sexe eq 'M'}">Monsieur</c:when>
				<c:when test="${sexe eq 'F'}">Madame</c:when>
				<c:otherwise>Indéterminé</c:otherwise>
			</c:choose>
		</h1>
		
		<c:if test="${empty prenoms}">Aucun prénom n'a été transmis</c:if>
		
		<%-- Comme une liste à puces --%>
		<ul>
			<c:forEach var="prenom" items="${prenoms}">
				<li>Prénom : ${prenom}</li>
			</c:forEach>
		</ul>
		
		<%-- Comme une table --%>
		<c:forEach varStatus="ligne" var="prenom" items="${prenoms}">
			<c:if test="${ligne.first}" > <%-- On met l'en-tête de la table --%>
				<table border="1">
					<tr><th>Numéro</th><th>Prénom</th></tr>	
			</c:if>
					<%-- une ligne dans la table pour chaque prénom --%>
					<tr><td>${ligne.count}<td>${prenom}</td></tr>
					
			<c:if test="${ligne.last}" > <%-- On ferme la table --%>
				</table>
			</c:if>
		</c:forEach>
		
		<hr>
		<a href="${pageContext.request.contextPath}">Retour au menu</a>	
	</body>
</html>
