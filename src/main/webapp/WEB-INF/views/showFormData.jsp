<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Formmulaire de saisie</title>
    </head>
    <body>
        <h1>Vous avez saisi :</h1>
	nom : ${data.nom}<br>
	email : ${data.email}<br>
	date de naissance : ${data.naissance}<br>
	salaire : ${data.salaire}<br>
	<hr>
	<a href="${pageContext.request.contextPath}">Retour au menu</a>	
    </body>
</html>
