package controller;

import form.SimpleBeanParam;
import form.ValidatingBeanParam;
import java.util.List;
import javax.inject.Inject;
import javax.mvc.Controller;
import javax.mvc.Models;
import javax.mvc.View;
import javax.mvc.binding.BindingResult;
import javax.validation.Valid;
import javax.validation.executable.ExecutableType;
import javax.validation.executable.ValidateOnExecution;
import javax.ws.rs.BeanParam;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

/**
 * Exemple de contrôleur
 * avec ou sans paramètre
 */
@Controller @Path("hello")
@View("hello.jsp")
public class HelloController {
	@Inject
	// Utilisé pour valider les données de formulaires
	BindingResult formValidationErrors;

	
	@Inject
	// Utilisé pour transmettre des informations
	// du contrôleur à la vue
	private Models models;

	@GET @Path("noParam")
	public void sayHello() {
		models.put("message", "World");
	}

	@GET @Path("queryParamExample")
	public void sayHelloTo(@QueryParam("name") String name) {
		models.put("message", name);
	}

	@GET @Path("defaultValue")
	public void sayHelloToDefault(
	   @DefaultValue("Inconnu") 
	   @QueryParam("name") String name) {
		models.put("message", name);
	}

	@GET @Path("formulaire")
	@View("formulaireHello.jsp")
	public void montreLeFormulaire() {
	}	

	@POST @Path("formulaire")
	public void traiteLesParametresDuFormulaire(@FormParam("sexe") String sexe) {
		String value = "Inconnu";
		switch(sexe) {
			case "M" : value = "Monsieur"; break;
			case "F" : value = "Madame"; break;			
		}
		models.put("message", value);		
	}	

	@GET @Path("jstl")
	@View("demoJSTL.jsp")
	public void exemplesJSTL(
		@DefaultValue("M") @QueryParam("sexe") String sexe,
		@QueryParam("prenom") List<String> prenoms		
	) { 
		models.put("sexe", sexe);
		models.put( "prenoms", prenoms);
	}	
	
	@GET @Path("header")
	// On accède à  un en-tête de la requête HTTP
	public void showHeader(@HeaderParam("User-Agent") String agent) {
		models.put("message", "navigateur: " +  agent);				
	}

	@GET @Path("cookie")
	// On accède à  un cookie transmis dans la requête HTTP
	public void showCookie(@CookieParam("JSESSIONID") String idSession) {
		models.put("message", "session n° " + idSession);				
	}

	@GET @Path("to/{name}")
	// le paramètre est extrait du chemin d'accès
	public void showPathParam(@PathParam("name") String name) {
		models.put("message", name);
	}	

	@GET @Path("beanParam")
	@View("beanForm.jsp")
	public void showBeanForm() {
		models.put("beanParam", new SimpleBeanParam()); // Valeurs par défaut		
		models.put("validationErrors", formValidationErrors);		
	}	

	@POST @Path("beanParam")
	public String getBeanForm(@BeanParam SimpleBeanParam data) {
		models.put("data", data);
		return "showFormData.jsp";
	}	

	@GET @Path("validBeanParam")
	@View("beanForm.jsp")
	public void showValidBeanForm() {
		models.put("beanParam", new ValidatingBeanParam()); // Valeurs par défaut		
		models.put("validationErrors", formValidationErrors);		
	}	

	@POST @Path("validBeanParam")
	@ValidateOnExecution(type = ExecutableType.ALL)		
	public String getValidBeanForm(@Valid @BeanParam ValidatingBeanParam data) {
		String view;
		if ( ! formValidationErrors.isFailed()) {
			models.put("data", data);	
			view = "showFormData.jsp"; // on montre les données saisies			
		} else {
			models.put("validationErrors", formValidationErrors);
			models.put("beanParam", data); // Valeurs soumises					
			view = "beanForm.jsp"; // On revient vers le formulaire en montrant les erreurs de saisie
		}
		return view;
	}	
}
