package form;

import javax.mvc.binding.MvcBinding;
import javax.ws.rs.FormParam;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import util.HtmlDate;

public class SimpleBeanParam implements Serializable {
	@MvcBinding @FormParam("nom")
	private String nom = "";

	@MvcBinding @FormParam("email")
	private String email = "";
	
	@MvcBinding @FormParam("naissance")
	//@DateFormat("yyyy-MM-dd")
	private HtmlDate naissance = new HtmlDate();
	
	@MvcBinding @FormParam("salaire")
	private BigInteger salaire = new BigInteger("1234");
	
	// Getters et setters

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getNaissance() {
		return naissance;
	}

	public void setNaissance(HtmlDate naissance) {
		this.naissance = naissance;
	}

	public BigInteger getSalaire() {
		return salaire;
	}

	public void setSalaire(BigInteger salaire) {
		this.salaire = salaire;
	}


}
