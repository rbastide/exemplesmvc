package form;

import java.math.BigInteger;
import java.util.Date;
import javax.mvc.binding.MvcBinding;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import javax.ws.rs.FormParam;
import util.HtmlDate;

/**
 * Utilisation de la validation de données
 * @See https://www.baeldung.com/javax-validation
 */
public class ValidatingBeanParam {
	@MvcBinding @FormParam("nom")
	@Size(min = 5, max = 25)
	@Pattern(regexp = "[A-Z][a-z]+", message = "Une majuscule suivie de minuscules")	
	private String nom = "";

	@MvcBinding @FormParam("email")
	@NotEmpty
	@Email // Doit avoir la forme d'une adresse email
	private String email = "";
	
	@MvcBinding @FormParam("naissance")
	@NotNull
	@Past // La date doit être dans le passé 
	private HtmlDate naissance = new HtmlDate();
	
	@MvcBinding @FormParam("salaire")
	@Positive
	@Max(value = 10000)
	private BigInteger salaire = new BigInteger("1234");
	
	// Getters et setters

	public String getNom() {
		return nom;
	}

	public void setNom(String chaine) {
		this.nom = chaine;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getNaissance() {
		return naissance;
	}

	public void setNaissance(HtmlDate naissance) {
		this.naissance = naissance;
	}

	public BigInteger getSalaire() {
		return salaire;
	}

	public void setSalaire(BigInteger salaire) {
		this.salaire = salaire;
	}		
}
