package config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configuration de l'application
 */
// Le chemin racine des contrôleurs MVC
@ApplicationPath("mvc")
public class ApplicationConfig extends Application {
}
